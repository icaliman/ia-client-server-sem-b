var fs = require('fs');
var libxmljs = require("libxmljs");

fs.readFile(__dirname + '/foo.xml', function(err, data) {
    if (err) {
        console.log(err);
        return;
    }
    
    var doc = libxmljs.parseXmlString(data);
    
    var entities = doc.find('//ENTITY');
    
    entities.forEach(function(it) {
        console.log("ENTYTY:");
        console.log("    ", it.text());
        console.log("    ", it.attr('ID').value());
    })
});



// var xmlDoc = libxmljs.parseXmlString(xml);


// var els = xmlDoc.find('*/grandchild');

// console.log(els[0].text())
// console.log(els[0].attr('baz').value())


// // xpath queries
// var gchild = xmlDoc.get('//grandchild');

// console.log(gchild.text());  // prints "grandchild content"

// var children = xmlDoc.root().childNodes();
// var child = children[0];

// console.log(child.attr('foo').value()); // prints "bar"
