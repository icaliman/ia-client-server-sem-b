$(function() {


    $('#newBookForm2').submit(function(event){

        var value = $('.modal .modal-body #inputGenerateQRCodes').val();
        if($.isNumeric(value)){
            if(value < 1 || value > 100){
                $('.form-error-message').show();
                event.preventDefault();
            }else{
                $('.modal .modal-body form').hide();
                var mesaj = "<div class='msg-error-div'><h3>Thank You!</h3><br><p>Your download will start automatically in a few moments.</p></div>";
                $('.modal .modal-body').append(mesaj);
                setTimeout(function(){
                    $('.msg-error-div').hide();
                    $('.form-error-message').hide();
                    $('.modal .modal-body form').show();
                },5000);
            }
        }else{
            $('.form-error-message').html("Please enter a number!").show();
            event.preventDefault();
        }
    
    });
    
    
    // SEARCH
    $('#search-book-input').keyup(function(){
        var leng = $(this).val().length;
        if(leng !== 0){
            $('.book-list-item').show();
            var qry = $(this).val();
            $(".book-list-item").each(function(){
                if($(this).html().toLowerCase().indexOf(qry.toLowerCase()) == -1){
                    $(this).hide();
                }
            });
        }else{
            $('.book-list-item').show();
        }
    });
    
    $('.book-list-item').click(function() {
      $('.book-list-item').removeClass('active');
      $(this).addClass('active');
      
      var id = $(this).attr('data-book-id');
      
      $('.delete-book-alert').hide();
      
      if ($('#book_'+id).length > 0) {
        $('.book-description').hide();
        $('#book_'+id).show();
      } else {
        getAjaxData('/admin/book/'+id, function(data) {
          $('.book-description').hide();
          var it = $(data);
          $('#book-info').append(it);
          it.find('#delete-book-button').click(function(){
              it.find('.delete-book-alert').show();
          });
          it.find('#close-btn-delete-book-alert').click(function(){
              it.find('.delete-book-alert').hide();
          });
        });
      }
    });
    
    $('#delete-book-button').click(function(){
        $('.delete-book-alert').show();
    });
    $('#close-btn-delete-book-alert').click(function(){
        $('.delete-book-alert').hide();
    });
});


function getAjaxData(address, next){
    jQuery.ajax({
        url: address,
        type: 'GET',
        success: function(response) {
            next(response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); 
            alert("Error: " + errorThrown); 
        } 
	});
}

function sendAjaxData(address, message){
    jQuery.ajax({
        url: address,
        type: 'POST',
        data: message,
        success: function(response) {
            return response;
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); 
            alert("Error: " + errorThrown); 
        } 
	});
}



