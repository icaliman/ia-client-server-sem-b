var sax = require('sax'),
    fs = require('fs'),
    geoloc = require('./geolocation'),
    strict = true,
    parser = sax.parser(strict),
    entityFLAG = false,
    locatii = [],
    temp = [];


/*
 location_info : obiect de tip { type: <type>, location: <location> },
 unde: type poate fi "url" sau "file".
       location - daca type e "url", link catre xml
                - daca type e "file, cale catre fisier (absoluta sau
                  relativa)
 callback: functia getXML e asincrona, deci callback o sa fie o
 functie cu signatura (err, xmldata), unde, daca err nu e null,
 xmldata contine un string cu tot xml-ul.
*/
function getXML(location_info, callback) {
  if (location_info.type == "file") {
    fs.readFile(location_info.location, function(err, data) {
      if (err)
        return callback(err);
      callback(null, data);
    });
  }

  if (location_info.type == "url") {
    //todo , daca e nevoie
    callback(new Error("not implemented"));
  }
}

parser.onopentag = function (node) {
  //daca am intrat intr un tag entity
  if (node.name == "ENTITY" && (node.attributes.TYPE == "locație" || node.attributes.TYPE == "instituție")) {
    entityFLAG = true; // setez tagul, sa vada si ontext()
    //creez un obiect pt tagul asta, in care memorez atributele
    var new_location = {
      attributes: node.attributes
    };
    //il pun pe stiva temporara
    temp.push(new_location);
  }
};


parser.ontext = function (t) {
  // daca sunt intr un tag ENTITY
  if (entityFLAG) {
    // curat textul de whitespace si de newlines...
    var locatie = t.trim().replace(/(\r\n|\n|\r)/gm,"");
    // daca dupa curatare, e vid, scot obiectul creat pt tag din stiva
    if (locatie.length == 0) {
     temp.pop();
      return;
    }
    // altminteri, ii adaug atributul name obiectului creat pt tag
    if(temp.length != 0) {
      temp[temp.length-1]['name'] = locatie;
    // il pun in lista de locatii
      locatii.push(temp[temp.length-1]);
    }
  }
};

parser.onclosetag = function(node) {
  if (node.name == "ENTITY")
    entityFLAG = false;
};


function getLocations(location_info, callback) {
    //HACK URAT, todo refacut
    if (parser.startTagPosition)
        parser = sax.parser(strict);
    // END HACK URAT
    
  getXML(location_info, function(err, data) {
    if (err) {
      callback(err);
      return;
    }
    
    var xml_string = data.toString('utf8');

    // sincron oare.. //semnulintrebarii
    parser.write(xml_string).close();

    geoloc.getGeoLocations(locatii, function(err, data) {
      if (err) {
        callback(err);
        return;
      }
      
      callback(null, data);
    });
  });
};

exports.getLocations = getLocations;

