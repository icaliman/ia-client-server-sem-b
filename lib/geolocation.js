// biblioteca pt geolocation prin google geocode api
// npm install geocoder
var geocoder = require('geocoder');
// bibliotea ajutatoare pt programare asincrona
// (pot sa nu o folosesc, dar codul a arata mai urat si ar fi mai
// susceptibil la erori.
// npm install async
var async = require('async');

function getGeoLocation(obiect_locatie, callback) {
  var nume_locatie = obiect_locatie.name;

  geocoder.geocode(nume_locatie, function (err, data) {
    if (err) {
      callback(err);
      return;
    }
    
    // MA INTERESEAZA DOAR PRIMUL REZULTAT.
    if (data.results.length === 0) {
      obiect_locatie['has_geocoords'] = false;
      return callback(null, obiect_locatie);      
    }


    var geoinfo = data.results[0].geometry.location;
    obiect_locatie['latitudine'] = geoinfo.lat;
    obiect_locatie['longitudine'] = geoinfo.lng;
    obiect_locatie['has_geocoords'] = true;
    callback(null, obiect_locatie);
  });
}


/*
 lista_locatii -- lista cu locatii
 callback(err, results) -- fiind functie asincrona, o sa revina instant, deci
 al doilea parametru este o functie care o sa fie apelata cu (err,
 rezultat), cand va termina totul. daca totul e ok, err o sa fie null
 si results o sa contina lista updatata cu geolocation
 */
function getGeoLocations(lista_locatii, callback) {
  // numarul magic 3 (param 2) repr numarul maxim de query-uri pe care
  // sa il fac la google api in paralel. 3 pare frumos, asa, ca in basme.
  async.mapLimit(lista_locatii,
                 3, 
                 getGeoLocation, 
                 function(err, locatii) {
                   if (err)
                     callback(err);
                   else
                     callback(null, locatii);
                 });
}

exports.getGeoLocations = getGeoLocations;
