module.exports = (app) ->
  require("./consts.coffee")(app)
  require("./passport.coffee")(app)
    
  # Helpers
  app.helpers = require "#{__dirname}/../app/helpers"

  # Lib
  app.helpers.autoload "#{__dirname}/../lib", app

  # Models
  app.helpers.autoload "#{__dirname}/../app/models", app

  # Controllers
  app.helpers.autoload "#{__dirname}/../app/controllers", app
  
  # Routes
  app.helpers.autoload "#{__dirname}/../app/routes", app
  
