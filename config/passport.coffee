module.exports = (app) ->

  global.passport = require('passport')
  LocalStrategy = require('passport-local').Strategy
  
  passport.use new LocalStrategy (username, password, done) ->
    console.log ">>>>>>> Admin login >>>>>> ", username, password, app.admin
    
    if app.admin.username!=username
      return done(null, false, { message: 'Incorrect username.' })
      
    if app.admin.password!=password
      return done(null, false, { message: 'Incorrect password.' })
      
    done null, app.admin
    
  passport.serializeUser (user, done) ->
    done null, user.id
  
  passport.deserializeUser (id, done) ->
    if id == app.admin.id
      return done null, app.admin
    dine "Incorrect user", null
    