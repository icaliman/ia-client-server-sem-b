module.exports = (app) ->
  require "./consts.coffee"
    
  # Helpers
  app.helpers = require "#{__dirname}/../app/helpers"

  # Lib
  app.helpers.autoload "#{__dirname}/../lib", app

  # Controllers
  app.helpers.autoload "#{__dirname}/../app/controllers", app
  
  # Routes
  app.helpers.autoload "#{__dirname}/../app/routes", app
  
  # Models
  app.helpers.autoload "#{__dirname}/../app/models", app
