exec = require('child_process').exec
fs = require('fs')

exports.execJar = (m, input, output, next) ->
  jarFile = __rootdir + "/jars/#{m}/#{m}.jar"
  cmd = "/usr/bin/java -jar #{jarFile} #{input} #{output}"
  
  console.log "------------------------------------------------"
  console.log "Exec JAR File: ", jarFile
  console.log "Input file: ", input
  console.log "Output file: ", output
  console.log "CMD : ", cmd
  
  exec cmd, (error, stdout, stderr) ->
    next(err)