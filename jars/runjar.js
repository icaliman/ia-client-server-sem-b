var exec = require('child_process').exec, child; 
var fs = require('fs');

exports.testJar = function(file, next) {
  var jar = __rootdir + '/jars/Hello.jar',
      output = __rootdir + '/jars/'+file;
  
  console.log("Run jar: ", jar);
  console.log("Output file: ", output);
      
  child = exec('/usr/bin/java -jar ' + jar + ' ' + output, 
    function (error, stdout, stderr){
      console.log('stdout: ' + stdout);
      console.log('stderr: ' + stderr);
      
      if(error !== null){
        console.log('exec error: ' + error);
      }
      
      fs.readFile(output, {encoding: 'utf-8'}, function(err, content) {
        next(error, stdout, stderr, {
          err: err,
          content: content
        });
      });
    }
  );
}