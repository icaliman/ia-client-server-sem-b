# Installing
***

To run server on your own machine, first install [Node.js](http://nodejs.org/) and [Git](http://git-scm.com/downloads).
The Node installer will also install [npm](http://npmjs.org/).

Then run these commands from cmd:

1. git clone git@bitbucket.org:icaliman/ia-client-server-sem-b.git
2. cd ia-client-server-sem-b && npm install
3. node server.js

Now go to [http://localhost:3000/](http://localhost:3000/)