mongoose = require('mongoose')
Schema = mongoose.Schema

Users = new Schema({
    username    : String,
    email       : String,
    password    : String,
    #ownedBooks  : { type: [Schema.ObjectId], ref: 'Books' }
    ownedBooks  : [{ type: Schema.ObjectId, ref: 'Books' }]
});

Books = new Schema({
    bookTitle   : String,
    bookAuthor  : String,
    bookYear    : String
});

QRCodes = new Schema({
    bookID   : { type: Schema.ObjectId, ref: 'Books' }
});

Place = new Schema({
    bookID  : { type: Schema.ObjectId, ref: 'Books' }
    name    : String
    #lat     : type: Number
    #lon     : type: Number
    ref     : String
    subtype : String
    geo     : {type: [Number], index: '2d'}
})

Pref = new Schema({
    sessionID   : String
    proximity    : {type: Number}
    #fontSize    : type: Number
    #mode        : type: String
});

mongoose.model('Users', Users)
mongoose.model('Books', Books)
mongoose.model('QRCodes', QRCodes)
mongoose.model('Pref', Pref)
mongoose.model('Place', Place)

#10079 10025
#mongoose.connect('mongodb://asdfqwer:1234qwer@linus.mongohq.com:10088/mapping-books') 
mongoose.connect('mongodb://asdfqwer:1234qwer@dharma.mongohq.com:10033/clientserver')


module.exports = (app) ->
  global.Acount = mongoose.model('Users')
  global.Books = mongoose.model('Books')
  global.QRCode = mongoose.model('QRCodes')
  global.Pref = mongoose.model('Pref') 
  global.Place = mongoose.model('Place')
