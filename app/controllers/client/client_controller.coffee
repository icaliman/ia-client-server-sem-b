#mongoose = require('mongoose')
#Acount   = mongoose.model('Users')
#Books     = mongoose.model('Books')
#QRCode   = mongoose.model('QRCodes')
#Place =  mongoose.model('Place')
#Pref =  mongoose.model('Pref')

module.exports = (app) ->
    class app.ClientController

        # Login
        # Params : (username | email) , password
        # Return : userSessionID
        # POST   : /client/login
        @login = (req, res, next) ->
        
            #Log Users
            #Users.find (err, users) ->
            #    return next(err)  if err
            #    console.log users
            
            console.log ">>>>> Client login: ", req.body
     
            Acount.findOne
                password: req.body.password
                $or: [
                    {username: req.body.username}
                    ,
                    {email: req.body.email}
                ]
            , (err, acount) ->
                console.log "error" if err
                if acount isnt null
                    console.log "succes"
                    res.send
                        status: 'ok',
                        sessionId: acount._id
                else 
                    console.log "wrong user or pass"
                    res.send
                        status: 'fail'
                        message: 'Wrong username or password'
 
                            
            
        # Register
        # Params: username , email , password
        # Return: userSessionID    
        # POST /client/register
            
        @register= (req, res,next) ->
            console.log "Username: %s - password: %s - email: %s", req.body.username, req.body.password, req.body.email

            Acount.findOne
                $or: [
                    {username: req.body.username}
                    ,
                    {email: req.body.email}
                ]
            , (err, acount) ->
                console.log "error" if err
                if acount isnt null
                    console.log "Username or email not available"
                    res.send
                        status: 'fail'
                        message: 'Username or email not available'
                else 
                    new Acount(
                        username: req.body.username
                        password: req.body.password
                        email: req.body.email
                        ).save (err, acount, count) ->
                            return next(err)  if err
                            console.log 'New user registered'
                            res.send
                                status: 'ok',
                                sessionId: acount._id

                    
        # Get preferences
        # Params: sessionID
        # Return: preferences
        # POST: /client/getPreferences  
        @getPreferences = (req, res, next) ->
            Acount.findById req.body.sessionID, (err, person) ->
              unless person
                return res.send
                  status: 'fail'
                  message: 'Invalid sessionID'
                  
              Pref.findOne 'sessionID': req.body.sessionID , (err,userPref) ->
                console.log "Get user prefs: ", userPref
                
                unless userPref
                    userPref = new Pref(
                        sessionID  : req.body.sessionID
                        proximity  : 1000
                    )
                    userPref.save()
                
                    console.log "New prefs: ", userPref
                    
                res.send
                    status: 'ok'
                    proximity: userPref.proximity
        
        # Set preferences
        # Params: sessionID, preferences
        # Return: ok / fail
        # POST: /client/setPreferences    
        @setPreferences = (req, res, next) ->
            Acount.findById req.body.sessionID, (err, person) ->
              unless person
                return res.send
                  status: 'fail'
                  message: 'Invalid sessionID'
                  
              Pref.findOne 'sessionID': req.body.sessionID , (err,userPref) ->
                  console.log 'User Pref'
                  console.log userPref
                  
                  if userPref
                      #userPref.mode = req.body.mode || userPref.mode
                      #userPref.fontSize = req.body.fontSize || userPref.fontSize
                      userPref.proximity = req.body.proximity || userPref.proximity
                      userPref.save()
                  
                  else
                      userPref = new Pref(
                          sessionID  : req.body.sessionID
                          proximity  : req.body.proximity || 1000
                      )
                      userPref.save()
                      
                  res.send
                      status  : 'ok'
                      message : 'preferences saved'
                          
        
        # Get Proximity Locations
        # Params: sessionID
        # Return: [proximityLocations]
        # POST: /client/getProximityLocations
        @getProximityLocations = (req, res, next) ->
            bookID = req.body.bookID
            sessionID = req.body.sessionID
            lat = req.body.lat
            long= req.body.long
            
            unless bookID and sessionID and lat and long
              return res.send
                status: 'fail'
                message: 'Incorrect parameters.'
              
            Acount.findById sessionID, (err, person) ->
              unless person
                return res.send
                  status: 'fail'
                  message: 'Invalid sessionID'
                  
              if person.ownedBooks.indexOf(bookID) == -1
                return res.send
                  status: 'fail'
                  message: 'User has not access to this book'
              
              Pref.findOne 'sessionID': req.body.sessionID , (err,userPref) ->
              
                geo = [lat, long] #[ <longitude> , <latitude>
                #geo.push long , lat #[ <longitude> , <latitude>
                
                #park = new Place(geo:geo, name:"locatie").save() 
                prefDistance = 1000 #meters
                if userPref
                  prefDistance = userPref.proximity
                
                console.log geo, prefDistance
                
                Place.find
                    bookID: bookID
                    geo:
                        $nearSphere: geo
                        $maxDistance: prefDistance/63711000 #1radian 63711km  #111120
                , (err, docs) ->
                    return next err if err
                    
                    res.json 
                      status: "ok"
                      locations: docs
                      
                    
        # Get User Books
        # Params: userSessionID
        # Return: A list of books [{ bookID , bookTitle }, ...]
        # POST: /client/getUserBooks
        
        @getUserBooks= (req, res, next) ->
          # loading books from DB for user with ID: userSessionID
          
          #books = []
          Acount.findById req.body.sessionID, (err, person) ->
                #return next(err)  if err
                console.log person
                
                unless person
                  return res.send
                    status: 'fail'
                    message: 'Incorrect sessionID.'
                
                if person.ownedBooks isnt undefined
                    console.log person.ownedBooks
                    
                    Books.find '_id': {$in: person.ownedBooks}, (err,books) ->
                        return next(err) if err
                        res.send
                            status: 'ok'
                            books: books
                
                else 
                    console.log "No books"
                    res.send
                        status: 'fail'
                        message: 'No books'
        
        # Get Book
        # Params : userSessionID , bookID
        # Return : bookContent (XML)
        # POST   : /client/getBook
        
        @getBook = (req, res, next) ->
            bookID = req.body.bookID
            sessionID = req.body.sessionID
            
            Acount.findById sessionID, (err, person) ->
              unless person
                return res.send
                  status: 'fail'
                  message: 'Invalid sessionID'
                  
              if person.ownedBooks.indexOf(bookID) == -1
                return res.send
                  status: 'fail'
                  message: 'User has not access to this book'
                
              
              fs = require("fs")
              fs.readFile __rootdir + "/book_xmls/" + bookID, "utf8", (err, data) ->
                  console.log "File was readed"
                  if err
                    return res.send
                      status: "fail"
                      message: "Error reading XML file: " + err
                  res.send 
                    status: 'ok'
                    xmlBook: data
                
                
                
        # Add Book
        # Params : userSessionID , QRCode
        # POST   : /client/addBook
        
        @addBook = (req, res, next) ->
                
            #Log Books and QRCodes
            #Books.find (err, books) ->
                #return next(err)  if err
                #console.log 'Books'
                #console.log books
                
            #QRCode.find (err, books) ->
                #return next(err)  if err
                #console.log 'QRCodes'
                #console.log books
                
            
            if (req.body.hasOwnProperty("qrcode")) and (req.body.hasOwnProperty("sessionID"))
  
                QRCode.findById req.body.qrcode, (err,qrcode) ->
                    #return next(err)  if err
                    if qrcode isnt null
                        Books.findById qrcode.bookID, (err, book) ->
                            #return next(err)  if err
                            if book isnt null
                                Acount.findById req.body.sessionID, (err, person) ->
                                    #return next(err)  if err
                                    
                                    unless person
                                      return res.send
                                        status: 'fail'
                                        message: 'Incorrect sessionID.'
                                    
                                    #verifica daca exista deja
                                    console.log person.ownedBooks.indexOf(book)
                                    if person.ownedBooks.indexOf(book._id) is -1
                                        person.ownedBooks.push book
                                        
                                        #todo: remove this qrcode
                                        person.save (err) ->
                                            return next(err) if err
                                            console.log 'Book saved to user'
                                            res.send
                                                status: 'ok'    
                                    else
                                        console.log "Books was added before"
                                        res.send
                                            status: 'fail'
                                            message: 'Your already own this book'
                            else 
                                #de ce exista un cod care nu are carte?
                                console.log 'book is null'
                                res.send
                                    status: 'fail'
                                    message: 'book is null'
                    else 
                        # probabil coduul a fost deja folosit
                        console.log 'QRCode is null'
                        res.send
                            status: 'fail'
                            message: 'qrcode is null'
                            
            # end first if - lipsa parametri
            else
                res.send
                    status: 'fail'
                    message: 'qrcode si sessionID sunt parametri obligatorii'
                

                
        # Get Locations
        # Params: userSessionID , bookID
        # Return: [locations]
        # POST: /client/getLocations
        @getLocations = (req, res, next) ->
            console.log req.body.sessionID
            
            Place.find
                bookID: req.body.bookID
            , (err, docs) ->
                unless err
                    res.json 
                      status: 'ok'
                      locations: docs
                else
                    res.json 
                      status: 'ok'
                      message: err
                    
                       
