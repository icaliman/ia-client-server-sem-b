locparse = require("../../../lib/locationParser")

module.exports = (app) ->
  fs = require 'fs.extra'
  
  class app.AdminController
  
    @editXML = (req, res) ->
      res.render 'editor'

    # GET /admin
    @adminPanel = (req, res) ->
      return res.redirect "/admin/login" unless req.user
      
      Books.find (err, books) ->
        return next(err) if err
        res.render "admin_index",
          books: books
    
    @bookDescription = (req, res) ->
      return res.redirect "/admin/login" unless req.user
      
      Books.findOne _id:req.params.id, (err, book) ->
        res.render 'admin_book',
          book: book
    
    @deleteBook = (req, res) ->
      return res.redirect "/admin/login" unless req.user
      
      Books.findOne _id:req.params.id, (err, book) ->
        #TODO: remove all book QRCodes
        #TODO: remove references from Users
        book?.remove()
        res.redirect '/admin'
    
    @users = (req, res) ->
      return res.redirect "/admin/login" unless req.user
      Acount.find  (err, acounts) ->
        return next err if err
        if acounts isnt null     
          console.log acounts
          res.render "admin_users",{users: acounts }
      

    @newBook = (req, res) ->
      return res.redirect "/admin/login" unless req.user
      
      unless req.body.bookTitle and req.body.author and req.body.year and req.files.coverImage and req.files.bookContent
        return res.redirect '/admin'
        
      book = new Books
        bookTitle: req.body.bookTitle,
        bookAuthor: req.body.author,
        bookYear: req.body.year
      book.save()
      
      coverImage = req.files.coverImage
      bookContent = req.files.bookContent
      
      imagePath = __rootdir + "/public/images/" + book._id
      if bookContent.type == "application/pdf"
        contentFilePath = __rootdir + "/book_xmls/" + book._id + ".pdf"
      else
        contentFilePath = __rootdir + "/book_xmls/" + book._id + ".txt"

      fs.move coverImage.path, imagePath, (err) ->
        return res.send err if err
        
        fs.move bookContent.path, contentFilePath, (err) ->
          return res.send err if err
          res.redirect ('/adnotare/1?contentFilePath=' + contentFilePath)
          return
          
          #Parsez cartea pentru locatii...
          locparse.getLocations
            type: "file"
            location: contentFilePath
            , (err, data) ->
                if err
                  console.log err
                else
                    #Am in data lista cu locatiile din carte
                    data.forEach (location) ->
                      geo = []
                      geo.push location.latitudine , location.longitudine
                      if location.has_geocoords is true
                        place = new Place
                          bookID: book._id,
                          name: location.name,
                          geo: geo
                          #lat: location.latitudine,
                          #lon: location.longitudine,
                          ref: location.attributes.REF,
                          subtype: location.attributes.SUBTYPE
                        place.save()

#          app.ParseBookXML book._id, contentFilePath
          
           res.redirect '/admin'
#          res.redirect '/adnotare/1?contentFilePath=' + contentFilePath
        
    # Generate QRCodes
    # Params : bookID
    # POST   : /admin/generateQRCodes     
    @generateQRCodes = (req, res, next) ->
      return res.redirect "/admin/login" unless req.user
      
      qr = require("qrcode-js")
      AdmZip = require('adm-zip')
      zip = new AdmZip()
      
      bookID = req.body.bookID
      count = parseInt(req.body.count)
      
      console.log ">>>>>>> ", req.body
      
      return res.redirect '/admin' if !count or count<0
      
      qrCodes = []
      
      i = count
      while i--
        code = new QRCode(bookID: bookID)
        code.save (err) ->
          if err then console.log ">>> Error saving QRCode: ", err
        console.log ">>> New QRCode: >>> ", i, " >> ", code._id
        
        qrcode = qr.toDataURL(code._id.toString(), 6).replace(/^data:image\/gif;base64,/,"")
        
        zip.addFile("qrcode_" + i + ".gif", new Buffer(qrcode, 'base64'), "A QRCode for acces to book.");
        
      willSendthis = zip.toBuffer()
      
      #fileName = __rootdir + "/public/zips/" + bookID + ".zip"
      #zip.writeZip(fileName)
      
      res.send willSendthis
      

    # Login
    # GET /admin/login
    @login = (req, res) ->
      res.render 'admin_login'
            
    # Body : username , password
    # POST   : /admin/login
    @loginAdmin = passport.authenticate 'local',
      successRedirect: '/admin',
      failureRedirect: '/admin/login'
        
    @logoutAdmin = (req, res) ->
      req.logout()
      res.redirect '/admin/login'
