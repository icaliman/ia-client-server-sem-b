locparse = require("../../lib/locationParser")

module.exports = (app) ->
    # Index
    app.get '/', app.ApplicationController.index
    
    app.get '/testgeo', (req, res) ->
        locparse.getLocations
            type: "file"
            location: "./xml_parser/tusnad.xml"
            , (err, data) ->
                console.log __dirname
                if err
                  console.log err
                  res.send "eroare"
                else
                    res.send data
                    
    app.get '/testjar/:file', (req, res) ->
        console.log __rootdir
        testJar = require(__rootdir+"/jars/runjar.js").testJar
        file = req.params.file
        
        execJar = require(__rootdir+"/jars").execJar
        
        execJar "m1", "input.xml", "output.xml", () ->
          console.log "OK"
        
        testJar file, (error, stdout, stderr, file) ->
          
          res.send
            error: error
            stdout: stdout
            stderr: stderr
            file: file
        
    # Error handling (No previous route found. Assuming it’s a 404)
    app.get '/*', (req, res) ->
        NotFound res
    
        
    NotFound = (res) ->
        res.render '404', status: 404, view: 'four-o-four'
