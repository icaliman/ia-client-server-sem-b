module.exports = (app) ->
    
    # Login
    # Params : (username | email) , password
    # Return : sessionID
    app.post '/client/login', app.ClientController.login
    
    # Register
    # Params : username , email , password
    # Return : sessionID
    app.post '/client/register', app.ClientController.register
    
    # Get User Books
    # Params : sessionID
    # Return : A list of books [{ bookID , bookTitle }, ...]
    app.post '/client/getUserBooks', app.ClientController.getUserBooks
    
    # Get Book
    # Params : sessionID , bookID
    # Return : bookContent (XML)
    app.post '/client/getBook', app.ClientController.getBook
    
    # Add Book
    # Params : userSessionID , QRCode
    # Return : message
    app.post '/client/addBook', app.ClientController.addBook

    # Get Proximity Locations
    # Params: sessionID, currentLocation, bookID
    # Return: [proximityLocations]
    app.post '/client/getProximityLocations', app.ClientController.getProximityLocations
    
    
    app.post '/client/setPreferences', app.ClientController.setPreferences
    app.post '/client/getPreferences', app.ClientController.getPreferences  
    
    app.post '/client/getLocations', app.ClientController.getLocations
    