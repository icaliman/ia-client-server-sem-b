var exec = require('child_process').exec, child; 
var fs = require('fs');

exports.incepeAdnotare = function(req, res) {
  var inPath = req.query.contentFilePath;
  var outPath =  __rootdir + '/jars/m1/out.xml';
  var jarPath = __rootdir + '/jars/m1/m1.jar';
//  var javaPath = '/opt/java/bin/java';
  var javaPath = '/usr/bin/java';
  var param3 = __rootdir + '/jars/m1';


  var cmd = javaPath + ' -jar ' + jarPath + ' ' + inPath + ''
        + ' ' + outPath + ' ' + param3;

  console.log("Rulez comanda: " + cmd);

  child = exec(cmd, 
               function (error, stdout, stderr){
                 //console.log('stdout: ' + stdout);
                 //console.log('stderr: ' + stderr);
                 
                 console.log("Am terminat de rulat comanda.");

                 fs.readFile(outPath, function(err, content) {
                   //console.log(content.toString());
                   res.render('editor', {xml_content: content.toString()});
                   //res.send(content);
                 });
                 if(error !== null){
                   console.log('exec error: ' + error);
                 }
                 console.log("DONE...");
               }
              );
};
