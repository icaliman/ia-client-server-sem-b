var exec = require('child_process').exec, child; 
var fs = require('fs');
var locparse = require('../../lib/locationParser');

exports.adnotare = function(req, res) {
  var jarId = req.params.jarNR;
  
  var xmlContent = req.body.xmlContent;  
  var fname = req.body.path;
  
  if (jarId == 666) {
    var bookID = fname.split(".")[0];
    var fileToDelete = __rootdir + '/book_xmls/' + fname;
    var newXML = __rootdir + '/book_xmls/' + bookID;

    console.log("Sterg fisierul: " + fileToDelete);
    fs.unlink(fileToDelete);
    console.log("Creez fisierul: " + newXML);
    fs.writeFileSync(newXML, xmlContent);

    console.log("Bag in bd locatiile din ... " + newXML);
    locparse.getLocations({
      type: "file",
      location: newXML
    }, function(err, data) {
      if (err) {
        return console.log(err);
      } else {
        return data.forEach(function(location) {
          var geo, place;
          geo = [];
          geo.push(location.latitudine, location.longitudine);
          if (location.has_geocoords === true) {
            place = new Place({
              bookID: bookID,
              name: location.name,
              geo: geo,
              ref: location.attributes.REF,
              subtype: location.attributes.SUBTYPE
            });
            place.save();
          }
        });
        console.log("GATA!!!!!!!!!!!!!!!!!!!!!!!!!...");
      }
    });
    res.send("DONE!");
  } else {
    if (parseInt(jarId) == 5) {
      var oldCWD = process.cwd();
      process.chdir(__rootdir + '/jars/m' + jarId);
      var inPath = "temp.xml";
      var outPath = "out.xml";      
    } else {
      var inPath =  __rootdir + '/jars/m' + jarId + "/temp.xml";
      var outPath =  __rootdir + '/jars/m' + jarId + "/out.xml";      
    }

    var jarPath = __rootdir + '/jars/m' + jarId + "/m" + jarId + ".jar";
//    var javaPath = '/opt/java/bin/java';
    var javaPath = '/usr/bin/java';
    
    var param3 = __rootdir + '/jars/m' + jarId;

    console.log("Scriu content in: " + inPath);

    fs.writeFile(inPath, xmlContent, function(err) {
      console.log("Am scris contentul in: " + inPath);
      console.log("Rulez jarul de pe calea: " + jarPath);
      
      var cmd = javaPath + ' -jar ' + jarPath + ' ' + inPath + ''
            + ' ' + outPath + ' ' + param3;

      console.log("Rulez comanda: " + cmd);
      console.log("AM CURDIR:" + process.cwd());
      child = exec(cmd, 
                   function (error, stdout, stderr){
                     //console.log('stdout: ' + stdout);
                     //console.log('stderr: ' + stderr);
                     
                     if (parseInt(jarId) == 5) {
                       process.chdir(oldCWD);
                     }
                     var outPath =  __rootdir + '/jars/m' + jarId + "/out.xml";      

                     console.log("Am terminat de rulat comanda.");

                     fs.readFile(outPath, function(err, content) {
                       res.end(content);
                     });
                     if(error !== null){
                       console.log('exec error: ' + error);
                     }
                     
                     // fs.readFile(output, {encoding: 'utf-8'}, function(err, content) {
                     //   next(error, stdout, stderr, {
                     //     err: err,
                     //     content: content
                     //   });
                     // });
                     console.log("DONE...");
                   }
                  );
    });
    
  }
};
