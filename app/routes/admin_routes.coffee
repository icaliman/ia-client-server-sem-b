incepeAdnotare = require("./incepeAdnotare").incepeAdnotare
adnotare = require("./adnotare").adnotare

module.exports = (app) ->
  # Login
  app.get '/admin/login', app.AdminController.login
  app.post '/admin/login', app.AdminController.loginAdmin
  app.get '/admin/logout', app.AdminController.logoutAdmin
  
  app.get '/admin/users', app.AdminController.users
  
  app.get '/admin/book/:id', app.AdminController.bookDescription
  
  app.get '/admin/delete/:id', app.AdminController.deleteBook
  
  app.post '/admin/newBook', app.AdminController.newBook

  app.get '/admin', app.AdminController.adminPanel
  
  app.post '/admin/generateQRCodes/:fileName', app.AdminController.generateQRCodes
  
  app.get '/admin/edit', app.AdminController.editXML

  app.get "/adnotare/1", incepeAdnotare
  app.post "/adnotare/:jarNR", adnotare
