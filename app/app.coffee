# Modules
express = require 'express'
passport = require 'passport'
http = require 'http'
app = express()

# Configuration
app.configure ->
  port = process.env.PORT || 3000
  if process.argv.indexOf('-p') >= 0
    port = process.argv[process.argv.indexOf('-p') + 1]

  app.set 'port', port
  app.set 'views', "#{__dirname}/views"
  app.set 'view engine', 'jade'
  app.use express.static("#{__dirname}/../public")
  app.use express.favicon()
  app.use express.logger('dev')
  app.use(express.cookieParser())
  app.use express.bodyParser({ keepExtensions: true })
  app.use express.methodOverride()
  app.use(express.session({ secret: 'keyboard cat aaaa' }))
  app.use require('connect-assets')(src: "#{__dirname}/assets")
  
  app.use passport.initialize()
  app.use passport.session()
  
  app.use app.router

app.configure 'development', ->
  app.use express.errorHandler()

# Boot setup
require("#{__dirname}/../config/boot")(app)

# Server
http.createServer(app).listen app.get('port'), ->
  console.log "Express server listening on port #{app.get 'port'} in #{app.settings.env} mode"
